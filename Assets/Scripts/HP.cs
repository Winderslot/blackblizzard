﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HP : MonoBehaviour
{
    public float maxHP;
    public float currentHp;
    

    // Start is called before the first frame update
    void Start()
    {
        currentHp = maxHP;        
    }

    // Update is called once per frame
    void Update()
    {
        if (currentHp <= 0)
        {
            Destroy(this);
        }
    }
}

